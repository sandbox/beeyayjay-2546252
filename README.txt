Twig 7 Foundation is a base theme. It is not intended to be used as is, but as a starting point for creating a custom theme.

It extends the Twig 7 Theme to implement the Zurb Foundation responsive framework using Sass. It uses npm and bower package
managers to install third party software such as the Foundation framework and Grunt task runner to put everything in the
right place and to compile scss files.

The theme is a thin layer over the Twig 7 base theme, tying it in to the responsive framework. It contains only one Drupal
template, page.tpl.twig, which extends Twig 7's page template to wrap each major section (header, footer, content, main
and secondary menus) separately, in order to allow Foundation rows and columns to be applied selectively to each section.

Requirements
Package Management and Development - These are required on the dev environment only.
npm (https://www.npmjs.com/)
Grunt (http://gruntjs.com/)
Bower (http://bower.io/)
Sass (http://sass-lang.com/)

Twig 7 Theme and Dependencies
Twig 7 Theme (https://www.drupal.org/sandbox/beeyayjay/2546242)
TFD7 (https://www.drupal.org/project/tfd7)
Twig (http://twig.sensiolabs.org/)
Libraries API (https://www.drupal.org/project/libraries)
X Autoload (https://www.drupal.org/project/xautoload)


Installation
1. Install the Twig 7 Theme and TFD according to the instructions in the Twig 7 Theme readme or the standard TFD7
installation instructions (http://tfd7.rocks/install).

2. Install all of the apps listed under Requirements above, if they're not already on your system.

3. Set up and use your local environment
From twig_7_foundation directory
To install:
   npm install && bower install && grunt bower

To update bower components:
   bower update && grunt bower

To automatically compile sass:
    grunt

From child theme directory (you can ignore this if you're planning to use your own sass compiler)
To install grunt sass compiler:
   npm install

To automatically compile sass:
    grunt


The Grid Template

The theme contains an include file, grid.tpl.twig, which, as the name suggests, provide a simple way to include a grid on
a page. Here's a basic example of a views template that prints an unformatted list of rows in a grid layout:

{% extends "twig_7_theme::views/views-view-unformatted.tpl.twig" %}
{% include "twig_7_foundation::views/inc/grid.tpl.twig" with {"columns":[4]} %}

This will display the view items in rows of three items each, using the standard Foundation 'row' and 'small-4' classes.

If you want to use different row widths for different screen sizes, you can add them to the "columns" array, e.g.
{"columns":[12,4,2]} will display one 12-width column on small screens, three 4-width columns on medium screens and
six 2-width columns on large screens.

If you want to print a grid of something other than the rows variable, you can use the optional 'item_array' parameter.
For example, if you have a multi-value field, you can print it's contents in a grid by creating a field template like this:

{% include "views/inc/grid.tpl.twig" with {"columns":[4], "item_array":items} %}
