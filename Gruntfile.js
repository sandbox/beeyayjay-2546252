module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'compressed',
          sourceMap: true
        },
        files: {
            'css/main.css': 'scss/main.scss'
        }
      }
    },

    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass']
      },
    },


    bower: {
      dev: {
        dest: '../../libraries',
        options: {
          expand: true,
          keepExpandedHierarchy: false,
          ignorePackages: ['jquery', ''],
          packageSpecific: {
            'foundation': {
              dest: 'js/vendor',
              expand: false,
              files: [
                'js/foundation.min.js'
              ]
            },
            'modernizr': {
              expand: false,
              dest: 'js/vendor',
              files: [
                'modernizr.js'
              ]
            }
          }
        }
      }
    }


  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-bower');

  grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['build','watch']);
};
